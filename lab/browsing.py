browsing_session = []

browsing_session.append(1)
browsing_session.append(2)
browsing_session.append(3)

print(browsing_session)

last = browsing_session.pop()
print(last)
print(browsing_session)
print("redirect", browsing_session)

if not browsing_session:
    print("disable")

# from collections import deque
# print(browsing_session)
# browsing_session = deque([])
# browsing_session.popleft()
# print(browsing_session)