#Solution
from pprint import pprint

sentence = "This is a common interview question"

# Dictionary
char_frequency = {}

# Unreadable Result
for char in sentence:
    if char in char_frequency:
        char_frequency[char] += 1
    else:
        char_frequency[char] = 1
# print(char_frequency)
# pprint(char_frequency, width=1)

print (sorted(char_frequency.items(), key=lambda kv:kv[1]))

pprint (sorted(char_frequency.items(), key=lambda kv:kv[1], reverse=True))

#To know which char most print
char_fequency_sorted = sorted(
    char_frequency.items(), 
    key=lambda kv:kv[1], 
    reverse=True)
    
print("Frequency Char", char_fequency_sorted[0])
