from pprint import pprint

kalimat = """Hi william 
    how are you? 
    how long you have been here?
    who is your wife?
    what do you do for living?
    """
hitung_character = {}

for char in kalimat:
    if char in hitung_character:
        hitung_character[char] += 1
    else:
        hitung_character[char] = 1

pprint(sorted(hitung_character.items(), key=lambda kv:kv[1]))

hitung_character_sorted = sorted(
    hitung_character.items(),
    key=lambda kv:kv[1],
    reverse=True
)

print(
    "Character yang sering muncul ialah: ", 
    hitung_character_sorted[1]
)