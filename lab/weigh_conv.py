weight = int(input('Weight: '))
lb_weight = weight * 0.45
kg_weight = weight / 0.45

pilihan = input('(L)bs or (K)g: ')

if pilihan.upper() == 'L':
    print (f"You are {lb_weight} kg")       #Formated print

elif pilihan.upper() == 'K':
    print(f"You are {kg_weight} pounds")    #Formated print

else:
    print('Please type L or K')