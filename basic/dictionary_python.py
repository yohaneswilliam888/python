# point = {"x":1,"y":2}
# point = dict(x=1, y=2)
# point["x"] = 10
# point["z"] = 20
# print(point)

# if "a" in point:
#     print(point["a"])
# print(point.get("a", 0))

# del point["x"]
# print(point)

# for key, value in point.items():
#     print(key, value)


# Dictionary Chomprehention

# Bad Script
# values = {}
# for x in range(5):
#     values.append(x * 2)

# Good Script
# Formula: [Expression for item in items]
values = {x: x * 2 for x in range(5)}   #Dictionary Chomprehention
print(values)
# values = (x )  #Dictionary Chomprehention


values = (x * 2 for x in range(10))
print(values)
for x in values:
    print(x)
