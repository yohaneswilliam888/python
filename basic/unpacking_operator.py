numbers = [1,2,3]
print(*numbers) #we use * to unpacking the operator

#bad script
print(1,2,3)

values = list(range(5))
print(values)

#Unpacking List Range
values = [*range(5), "Hello"]
print(values)

first = [1,2]
second = [3]
values = [*first, "a", *second, *""]
print(*values)

# Unpacking Dictionary

first = {"x": 1}
second = {"x": 10, "y": 2}
combined = {**first, **second, "z": 1}
print(combined)
