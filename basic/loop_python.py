#for number in range (11):
#    print("Attempt", number, number * ".")

successful = False
for number in range (3):
    print("Attempt")
    if successful:
        print("Successful")
        break
else:
    print("Attempted 3 times and failed")


#Nested Loop

for x in range (5):
    for y in range (3):
        print(f"({x,y})")

print(type(5))
print(type(range(5)))

#Iterable
for x in [1,2,3,4]:
    print(x)

#While loop
number = 100
while number > 0:
    print(number)
    number //= 2

command = ""
#while command.lower() != "quit":
while True:
    command = input(">")
    print("Echo", command)
    if command.lower() == "quit":
        break
