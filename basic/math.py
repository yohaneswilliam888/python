import math         #Import math module

print (10 / 3) #result will be int
print (10 // 3) #result will be float

#incrementing number

x = 10
#x = x + 3
x *= 3 #+= -= *= /= **= //=
print (x)


x = 2.9 
print (round(x))    #round will make the number to be Int
print (abs(-x))     #abs will change the minus number to positive number

print (math.floor(x)) #This use math module