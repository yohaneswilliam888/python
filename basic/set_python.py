numbers = [1,1,1,2,3,3,4,4,3,5,5,6,6,4,6,7]
remove_duplicate = set(numbers)
add_numbers = {6,9,10,8}

combine = remove_duplicate | add_numbers

combine.add(11)
print(combine)
combine.remove(1)
print(combine)
