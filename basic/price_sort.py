item = [
    ("Aqua", 1),
    ("Susu", 6),
    ("Telur", 4),
    ("Tepung", 2),
]


item.sort(key=lambda item:item[1])
print(item)

# #Bad Map Function
# prices = list(map(lambda item:item[1], item))
# print(prices)

#Good Map Function
prices2 = [item[1] for item in item] 
print(prices2)

# #BAD Filter Lambda
# filtered = list(filter(lambda item: item[1] >= 4, item))
# print(filtered)

#Good Filter
filtered2 = [item for item in item if item[1] >= 4 ]
print(filtered2)

item.sort(reverse=True)
print(item)