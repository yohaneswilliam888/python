number = [5,2,1,7,4]
number.sort()       #asending the number
print(number)

number.reverse()    #decending the number
print(number)

number.append(20)   #add value 20 to the last index
print(number)

number.insert(2,10) #insert value 10 to 2 index
print(number)

number.remove(10)   #remove value 10
print(number)

number.pop()        #remove latest list
print(number)

print(21 in number) #Boolean value

number2 = number.copy()
print(number2)