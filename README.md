# Python

You can install python on linux using below command:  
**sudo apt install python**  
**python --version**  

Below how to change Python Version:

Please check basic folder for basic script that Python:  
**alias python='/usr/bin/python3.8'**  
**. ~/.bashrc**  
python --version  

Primitive type in Python are:
1. Number (Int, Float)
2. String
3. Boolean

**Ternary Operator:**
message = "Eligible" if >= 18 else "Not Eligible"
print(message)